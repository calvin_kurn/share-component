import React from "react";

// styled component
import { MainWrapper, Title, Highlight } from "./WelcomeStyle";

class Welcome extends React.Component {
	render() {
		return (
			<MainWrapper>
				<Title>Travel Component Storybook</Title>
				<ul>
					<li>
						Travel share component documentation, travel-share-component {""}
						<a href="https://www.npmjs.com/package/travel-share-component" target="_blank">
							https://www.npmjs.com/package/travel-share-component
						</a>
					</li>
					<li>
						Each component have <Highlight>isMobile</Highlight> props for mobile
					</li>
				</ul>
			</MainWrapper>
		);
	}
}

export default Welcome;
