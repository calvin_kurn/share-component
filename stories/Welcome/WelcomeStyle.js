import styled from "react-emotion";

const MainWrapper = styled("div")`
	margin: 24px auto;
	width: 800px;
	padding: 24px;
	border: 1px solid #e0e0e0;
	border-radius: 4px;

	a {
		color: #42b549;
		text-decoration: none;
	}

	ul {
		list-style: square;

		li {
			margin: 16px 0;
		}
	}
`;

const Title = styled("div")`
	font-weight: 600;
	font-size: 20px;
`;

const Highlight = styled("span")`
	color: red;
`;

export { MainWrapper, Title, Highlight };
