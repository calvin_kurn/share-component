import React from "react";
import { storiesOf } from "@storybook/react";
import TabContent from "unify-react-mobile/build/TabContent";
import {
	withKnobs,
	text,
	boolean,
	number,
	button,
	array
} from "@storybook/addon-knobs";

// component import
import {
	FlightSearch,
	FlightSearchNote,
	Button,
	Card,
	Chip,
	Tab
} from "../src";
import Welcome from "./Welcome";

function onClick(param) {
	alert(`Click from ${param}`);
}

function onClose(param) {
	alert(`Close from ${param}`);
}

// add component stories
storiesOf("Travel", module).add("Welcome", () => <Welcome />);

let suggestion = [
	{
		airports: [
			{
				id: "CGK",
				name: [
					{
						key: 0,
						value: "Soekarno-Hatta"
					}
				]
			},
			{
				id: "HLP",
				name: [
					{
						key: 0,
						value: "Halim Perdanakusuma"
					}
				]
			}
		],
		cityName: [
			{
				key: 0,
				value: "Jakarta"
			}
		],
		cityID: 57,
		code: "JKTA",
		countryID: "ID",
		countryName: [
			{
				key: 0,
				value: "Indonesia"
			}
		],
		name: [
			{
				key: 0,
				value: "Jakarta"
			}
		]
	},
	{
		airports: [],
		cityName: [
			{
				key: 0,
				value: "Jakar"
			}
		],
		cityID: 1112,
		code: "BUT",
		countryID: "BT",
		countryName: [
			{
				key: 0,
				value: "Bhutan"
			}
		],
		name: [
			{
				key: 0,
				value: "Bathpalathang"
			}
		]
	},
	{
		airports: [],
		cityName: [
			{
				key: 0,
				value: "Jacmel"
			}
		],
		cityID: 3022,
		code: "JAK",
		countryID: "HT",
		countryName: [
			{
				key: 0,
				value: "Haiti"
			}
		],
		name: [
			{
				key: 0,
				value: "Jacmel"
			}
		]
	}
];

storiesOf("Flight Widget", module)
	.add(
		"Desktop",
		() => (
			<div style={{ margin: "16px" }}>
				<FlightSearch
					submitValue={(value, flag) => {
						console.warn(value);
					}}
					destinationTyping={(id, value) => {
						// console.warn(`${id} - ${value}`);
					}}
					defaultDate={{
						departureDate: new Date(),
						returnDate: new Date()
					}}
					defaultDestination={{
						departure: { airportCode: "CGK", cityName: "Jakarta" },
						return: { airportCode: "DPS", cityName: "Bali" }
					}}
					airportList={suggestion}
				/>
			</div>
		),
		FlightSearchNote
	)
	.add(
		"Mobile",
		() => (
			<FlightSearch
				defaultPassengerClass={0}
				defaultDate={{
					departureDate: new Date(2019, 5, 20),
					returnDate: new Date(2019, 5, 25)
				}}
				defaultDestination={{
					departure: { airportCode: "CGK", cityName: "Jakarta" },
					return: { airportCode: "DPS", cityName: "Bali" }
				}}
				defaultPassenger={{ adult: 2, child: 0, infant: 0 }}
				submitValue={(value, flag) => {
					console.warn(value);
				}}
				isMobile
				destinationTyping={(id, value) => {
					console.warn(`${id} - ${value}`);
				}}
			/>
		),
		FlightSearchNote
	);

storiesOf("Unify", module)
	.addDecorator(withKnobs)
	.add("Button", () => (
		<Button
			isMobile={boolean("isMobile", false)}
			className={text("className", "custom_className")}
			disabled={boolean("disabled", false)}
			filled={boolean("filled", false)}
			floating={boolean("floating", false)}
			ghost={boolean("ghost", false)}
			image={text("image", null)}
			inverted={boolean("inverted", false)}
			large={boolean("large", false)}
			loading={boolean("loading", false)}
			main={boolean("main", false)}
			medium={boolean("medium", false)}
			micro={boolean("micro", false)}
			primary={boolean("primary", false)}
			secondary={boolean("secondary", false)}
			secondaryGreen={boolean("secondaryGreen", false)}
			secondaryTransaction={boolean("secondaryTransaction", false)}
			small={boolean("small", false)}
			text={boolean("text", false)}
			transaction={boolean("transaction", false)}
			type={text("type", "button")}
			onClick={() => onClick("Button")}
		>
			Transaction Small
		</Button>
	))
	.add("Card", () => (
		<Card
			margin={text("margin", "8px")}
			padding={text("padding", "8px")}
			onClick={() => onClick("Card")}
		>
			{text("child", "Child from Card Component")}
		</Card>
	))
	.add("Chip", () => (
		<Chip
			disabled={boolean("disabled", false)}
			active={boolean("active", true)}
			onClose={() => onClose("Chip")}
			onClick={() => onClick("Chip")}
			loading={boolean("loading", false)}
		>
			{text("child", "Child from Chip Component")}
		</Chip>
	))
	.add("Tab", () => (
		<Tab
			indexActive={number("indexActive", 0)}
			items={[
				{ key: 0, text: "Menu 0", value: 1000 },
				{ key: 1, text: "Menu 1", value: 1001 }
			]}
			onItemClick={(key, e) => onClick(key)}
		>
			<TabContent>
				<h1>Menu1 Content</h1>
			</TabContent>
			<TabContent>
				<h1>Menu2 Content</h1>
			</TabContent>
		</Tab>
	));
