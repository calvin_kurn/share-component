const path = require("path");
var nodeExternals = require("webpack-node-externals");

module.exports = {
    entry: path.join(__dirname, "src/index.js"),
    target: "node",
    externals: [nodeExternals()],
    output: {
        path: __dirname + "/build",
        filename: "index.js",
        library: "Travel",
        libraryTarget: "umd",
        umdNamedDefine: true
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    resolve: {
        extensions: [".js", ".jsx"]
    },
    devServer: {
        port: 3001
    }
};
