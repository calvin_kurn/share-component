import React from "react";

export default function unifyWrapper(MobileComponent, DesktopComponent) {
	return class extends React.Component {
		render() {
			if (this.props.isMobile) {
				return <MobileComponent {...this.props} />;
			} else {
				return <DesktopComponent {...this.props} />;
			}
		}
	};
}

