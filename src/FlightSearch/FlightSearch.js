import React from "react";

// self styling
import {
	MainWrapper,
	Button,
	LayoutTable,
	CheckBox,
	DateWrapper,
	// mobile
	MobileWrapper,
	MobileCity,
	ChipWrapper
} from "./FlightSearchStyle.js";

// external component
import { SearchCityCombine } from "./SearchCity";
import SearchPassenger from "./SearchPassenger";
import SearchClass from "./SearchClass";
import SearchDate from "./SearchDate";

// unify component
import Chip from "unify-react-mobile/build/Chip";

class FlightSearch extends React.PureComponent {
	constructor(props) {
		super(props);

		/**
		 * normalize all date format (without time)
		 * to clearly compare process
		 **/
		let normalizeDepartureDate;
		let normalizeReturnDate;

		if (props.date) {
			const departureDate = props.date.departureDate;
			const returnDate = props.date.returnDate;

			/*
			 ** re-create date object.
			 ** obj references from client (props) got diff calculation on date.getTime()
			 */
			normalizeDepartureDate = new Date(
				departureDate.getFullYear(),
				departureDate.getMonth(),
				departureDate.getDate()
			);
			normalizeReturnDate = new Date(
				returnDate.getFullYear(),
				returnDate.getMonth(),
				returnDate.getDate()
			);
		} else if (props.defaultDate) {
			const defaultDepartureDate = props.defaultDate.departureDate;
			const defaultReturnDate = props.defaultDate.returnDate;

			/*
			 ** re-create date object.
			 ** obj references from client (props) got diff calculation on date.getTime()
			 */
			normalizeDepartureDate = new Date(
				defaultDepartureDate.getFullYear(),
				defaultDepartureDate.getMonth(),
				defaultDepartureDate.getDate()
			);
			normalizeReturnDate = new Date(
				defaultReturnDate.getFullYear(),
				defaultReturnDate.getMonth(),
				defaultReturnDate.getDate()
			);
		} else {
			const today = new Date();
			normalizeDepartureDate = new Date(
				today.getFullYear(),
				today.getMonth(),
				today.getDay() - 1
			);
			normalizeReturnDate = new Date(
				today.getFullYear(),
				today.getMonth(),
				today.getDay() + 1
			);
		}

		normalizeReturnDate.setHours(0, 0, 0);
		normalizeDepartureDate.setHours(0, 0, 0);

		let tempDate = {
			departureDate: normalizeDepartureDate,
			returnDate: normalizeReturnDate
		};

		/**
		 * control default props from param state
		 **/
		this.state = {
			isRange: props.isRange || props.defaultIsRange || true,
			isBottomSheetOpen: false,
			param: {
				passengerClass:
					props.passengerClass || props.defaultPassengerClass || 1,
				date: tempDate,
				passenger: props.passenger ||
					props.defaultPassenger || { adult: 1, child: 0, infant: 0 },
				destination: props.destination ||
					props.defaultDestination || {
						departure: { airportCode: "", cityName: "" },
						return: { airportCode: "", cityName: "" }
					}
			}
		};

		this.destinationChanged = this.destinationChanged.bind(this);
		this.dateChanged = this.dateChanged.bind(this);
		this.passengerChanged = this.passengerChanged.bind(this);
		this.classChanged = this.classChanged.bind(this);
		this.toggleRoundTrip = this.toggleRoundTrip.bind(this);
		this.submitValue = this.submitValue.bind(this);
		this.onDestinationTyping = this.onDestinationTyping.bind(this);
	}

	static getDerivedStateFromProps(props, state) {
		const newProps = Object.assign({}, state.param);
		let isNewProps = false;

		// check if any changes on passengerClass Props
		if (
			props.passengerClass &&
			props.passengerClass !== state.param.passengerClass
		) {
			newProps["passengerClass"] = props.passengerClass;
			isNewProps = true;
		}

		// check if any changes on date Props
		if (props.date) {
			let tempDepartureDate = state.param.date.departureDate;
			let tempReturnDate = state.param.date.returnDate;

			if (
				props.date.departureDate.getTime() !==
				state.param.date.departureDate.getTime()
			) {
				tempDepartureDate = props.date.departureDate;
				isNewProps = true;
			}

			if (
				props.date.returnDate.getTime() !==
				state.param.date.returnDate.getTime()
			) {
				tempReturnDate = props.date.returnDate;
				isNewProps = true;
			}

			newProps["date"] = {
				departureDate: tempDepartureDate,
				returnDate: tempReturnDate
			};
		}

		// check if any changes on destination
		if (props.destination) {
			let tempDepartureDestination = state.param.destination.departure;
			let tempReturnDestination = state.param.destination.return;

			if (
				props.destination.departure.airportCode !==
				tempDepartureDestination.airportCode
			) {
				tempDepartureDestination = props.destination.departure;
				isNewProps = true;
			}

			if (
				props.destination.return.airportCode !==
				tempReturnDestination.airportCode
			) {
				tempReturnDestination = props.destination.return;
				isNewProps = true;
			}

			newProps["destination"] = {
				departure: tempDepartureDestination,
				return: tempReturnDestination
			};
		}

		// check if any changes on passenger
		if (props.passenger) {
			if (
				props.passenger.adult !== state.param.passenger.adult ||
				props.passenger.child !== state.param.passenger.child ||
				props.passenger.infant !== state.param.passenger.infant
			) {
				isNewProps = true;
				newProps["passenger"] = props.passenger;
			}
		}

		if (isNewProps) {
			// console.log(newProps);
			return { param: newProps };
		}
		return null;
	}

	classChanged(newClass) {
		const paramClone = Object.assign({}, this.state.param);
		paramClone.passengerClass = newClass;

		this.setState({
			param: paramClone
		});

		if (this.props.classChanged) this.props.classChanged(newClass);
	}

	destinationChanged(newDestination) {
		const paramClone = Object.assign({}, this.state.param);
		paramClone.destination = newDestination;

		this.setState({
			param: paramClone
		});

		if (this.props.destinationChanged)
			this.props.destinationChanged(newDestination);
	}

	dateChanged(newDepartureDate, newReturnDate, rangeFlag) {
		const paramClone = Object.assign({}, this.state.param);
		paramClone.date = {
			departureDate: newDepartureDate,
			returnDate: newReturnDate
		};

		this.setState({
			param: paramClone,
			isRange: rangeFlag
		});

		if (this.props.dateChanged)
			this.props.dateChanged(newDepartureDate, newReturnDate, rangeFlag);
	}

	passengerChanged(newPassenger) {
		const paramClone = Object.assign({}, this.state.param);
		paramClone.passenger = newPassenger;

		this.setState({
			param: paramClone
		});

		if (this.props.passengerChanged) this.props.passengerChanged(newPassenger);
	}

	toggleRoundTrip(evt) {
		const id = evt.currentTarget.id;
		this.setState(prev => ({
			isRange: id == 1
		}));
	}

	/**
	 * mobile handler
	 **/

	submitValue() {
		if (this.props.submitValue)
			this.props.submitValue(this.state.param, this.state.isRange);
	}

	onDestinationTyping(id, value) {
		const { destinationTyping } = this.props;
		const {
			param: {
				destination: { departure: departureValue, return: returnValue }
			}
		} = this.state;

		if (destinationTyping) {
			destinationTyping(id, value);
		}

		let tempResult;
		if (id === 1) {
			tempResult = {
				departure: { cityName: value, airportCode: "" },
				return: {
					cityName: returnValue.cityName,
					airportCode: returnValue.airportCode
				}
			};
		} else {
			tempResult = {
				departure: {
					cityName: departureValue.cityName,
					airportCode: departureValue.airportCode
				},
				return: { cityName: value, airportCode: "" }
			};
		}

		const paramClone = Object.assign({}, this.state.param);
		paramClone.destination = tempResult;

		this.setState({
			param: paramClone
		});
	}

	render() {
		const { isMobileRange, isBottomSheetOpen, param, isRange } = this.state;
		const { isMobile, airportList, popularList, passengerHandler } = this.props;
		const {
			classChanged,
			destinationChanged,
			dateChanged,
			passengerChanged,
			toggleRoundTrip,
			mobileFormClicked,
			submitValue,
			onDestinationTyping
		} = this;

		if (!isMobile) {
			return (
				<MainWrapper>
					<LayoutTable>
						<tbody>
							<tr>
								<th width="18%">Dari</th>
								<th width="3%" />
								<th width="18%">Tujuan</th>
								<th width="22%" colSpan="2" rowSpan="2">
									<SearchDate
										date={param.date}
										onChange={dateChanged}
										isRangePicker={isRange}
									/>
								</th>
								<th width="23%">Penumpang</th>
								<th width="15%">Kelas</th>
							</tr>
							<tr>
								<td colSpan="3">
									<SearchCityCombine
										destination={param.destination}
										airportList={airportList}
										popularList={popularList}
										onChange={destinationChanged}
										destinationTyping={onDestinationTyping}
									/>
								</td>
								<td>
									<SearchPassenger
										passenger={param.passenger}
										onChange={passengerChanged}
									/>
								</td>
								<td>
									<SearchClass
										passengerClass={param.passengerClass}
										onChange={classChanged}
									/>
								</td>
							</tr>
							<tr>
								<td colSpan="5" />
								<td colSpan="2">
									<Button
										float="right"
										display="inline-block"
										width="240px"
										onClick={submitValue}
									>
										Cari Tiket
									</Button>
								</td>
							</tr>
						</tbody>
					</LayoutTable>
				</MainWrapper>
			);
		} else {
			return (
				<MobileWrapper>
					<ChipWrapper>
						<Chip active={!isRange} onClick={toggleRoundTrip} id={0}>
							Sekali Jalan
						</Chip>
						<Chip active={isRange} onClick={toggleRoundTrip} id={1}>
							Pulang Pergi
						</Chip>
					</ChipWrapper>
					<SearchCityCombine
						destination={param.destination}
						airportList={airportList}
						popularList={popularList}
						onChange={destinationChanged}
						isMobile
						destinationTyping={onDestinationTyping}
					/>
					<SearchDate
						date={param.date}
						onChange={dateChanged}
						isRangePicker={isRange}
						isMobile
					/>
					<SearchPassenger
						passenger={param.passenger}
						onChange={passengerChanged}
						isMobile
					/>
					<SearchClass
						passengerClass={param.passengerClass}
						onChange={classChanged}
						isMobile
					/>
					<Button isMobile onClick={submitValue}>
						Cari Tiket
					</Button>
				</MobileWrapper>
			);
		}
	}
}

FlightSearch.defaultProps = {
	destinationTyping: () => {}
};

export default FlightSearch;
