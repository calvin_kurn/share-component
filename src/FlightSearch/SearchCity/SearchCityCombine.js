import React from "react";
import {
	MainWrapper,
	SwapIcon,
	MobileCity,
	SwapIconMobile,
	MobileWrapper
} from "./SearchCityCombineStyle";

import SearchCity from "./SearchCity";

// unify component
import BottomSheet from "unify-react-mobile/build/BottomSheet";

class SearchCityCombineStyle extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isRotate: false,
			departureDestination: props.destination
				? props.destination.departure
				: {
						cityName: "",
						airportCode: null
				  },
			returnDestination: props.destination
				? props.destination.return
				: {
						cityName: "",
						airportCode: null
				  },
			isMobileShowSheet: false,
			selectState: 1
		};

		this.toggleSwapDestination = this.toggleSwapDestination.bind(this);
		this.destinationChange = this.destinationChange.bind(this);
		this.toggleSheet = this.toggleSheet.bind(this);
	}

	static getDerivedStateFromProps(props, state) {
		if (!props.destination) return null;
		if (
			props.destination.departure.airportCode !==
				state.departureDestination.airportCode ||
			props.destination.return.airportCode !==
				state.returnDestination.airportCode
		) {
			return {
				departureDestination: props.destination.departure,
				returnDestination: props.destination.return
			};
		}
		return null;
	}

	toggleSwapDestination() {
		const { departureDestination, returnDestination } = this.state;

		// check if selected city empty or no
		if (
			departureDestination.airportCode === null ||
			returnDestination.airportCode === null
		) {
			return 0;
		}

		this.setState(
			prev => ({
				isRotate: !prev.isRotate,
				departureDestination: prev.returnDestination,
				returnDestination: prev.departureDestination
			}),
			this.props.onChange({
				departure: returnDestination,
				return: departureDestination
			})
		);
	}

	destinationChange(id, newDestination) {
		const { departureDestination, returnDestination } = this.state;
		let tempDeparture = departureDestination;
		let tempReturn = returnDestination;

		if (id == 1) {
			tempDeparture = newDestination;
		} else {
			tempReturn = newDestination;
		}

		this.setState(
			{
				departureDestination: tempDeparture,
				returnDestination: tempReturn,
				isMobileShowSheet: false
			},
			this.props.onChange({ departure: tempDeparture, return: tempReturn })
		);
	}

	toggleSheet(evt) {
		let id = evt.currentTarget.id;
		this.setState(prev => ({
			isMobileShowSheet: !prev.isMobileShowSheet,
			selectState: id
		}));
	}

	render() {
		const {
			isMobile,
			airportList,
			popularList,
			destinationTyping
		} = this.props;
		const {
			isRotate,
			departureDestination,
			returnDestination,
			isMobileShowSheet,
			selectState
		} = this.state;
		const {
			toggleSwapDestination,
			destinationChange,
			mobileClick,
			toggleSheet
		} = this;

		if (!isMobile) {
			return (
				<MainWrapper>
					<tbody>
						<tr>
							<td width="45%">
								<SearchCity
									airportList={airportList}
									popularList={popularList}
									column={3}
									maxHeight={350}
									maxWidth={250}
									suggestionWidth={500}
									id={1}
									onChange={destinationChange}
									value={departureDestination}
									onTyping={destinationTyping}
								/>
							</td>
							<td width="10%">
								<SwapIcon isRotate={isRotate} onClick={toggleSwapDestination} />
							</td>
							<td width="45%">
								<SearchCity
									airportList={airportList}
									popularList={popularList}
									column={3}
									maxHeight={350}
									maxWidth={250}
									suggestionWidth={500}
									id={2}
									onChange={destinationChange}
									value={returnDestination}
									onTyping={destinationTyping}
								/>
							</td>
						</tr>
					</tbody>
				</MainWrapper>
			);
		} else {
			return (
				<React.Fragment>
					<MobileWrapper>
						<MobileCity>
							<div id={1} onClick={toggleSheet}>
								<div className="title">Dari</div>
								<div className="airport_code">
									{departureDestination.airportCode || "-"}
								</div>
								<div className="city_name">
									{departureDestination.cityName || "-"}
								</div>
							</div>
							<SwapIconMobile>
								<SwapIcon
									isRotate={isRotate}
									onClick={toggleSwapDestination}
									isMobile
								/>
							</SwapIconMobile>
							<div id={2} onClick={toggleSheet}>
								<div className="title">Tujuan</div>
								<div className="airport_code">
									{returnDestination.airportCode || "-"}
								</div>
								<div className="city_name">
									{returnDestination.cityName || "-"}
								</div>
							</div>
						</MobileCity>
					</MobileWrapper>
					<BottomSheet
						full
						padding="0"
						title={
							selectState == 1 ? "Penerbangan Pergi" : "Penerbangan Pulang"
						}
						onClose={toggleSheet}
						display={isMobileShowSheet}
					>
						<SearchCity
							id={selectState}
							onChange={destinationChange}
							airportList={airportList}
							popularList={popularList}
							value={
								selectState == 1 ? departureDestination : returnDestination
							}
							isMobile
							onTyping={destinationTyping}
						/>
					</BottomSheet>
				</React.Fragment>
			);
		}
	}
}

export default SearchCityCombineStyle;
