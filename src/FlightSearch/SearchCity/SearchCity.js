import React from "react";
import mouseCheckOutside from "../helper.js";
import {
	TextBox,
	MainWrapper,
	SearchSuggestion,
	PopularDestination,
	PopularTitle,
	PopularContent,
	PopularCountry,
	PopularCity,
	SuggestionList,
	SuggestionItem,
	ColumnWrapper,
	MobileInput,
	MobileSuggestionWrapper
} from "./SearchCityStyle.js";

import { listData, popularData } from "./airport";

class SearchCity extends React.Component {
	constructor(props) {
		super(props);

		const defaultCityName = props.value.name || null;
		const defaultCityCode = props.value.code || null;

		this.state = {
			isSuggestionOpen: false,
			isPopularShow: true,
			selectedCity: {
				cityName: defaultCityName,
				airportCode: defaultCityCode
			},
			textBoxValue: "",
			airportList: [],
			popularList: []
		};

		this.toggleSuggetion = this.toggleSuggetion.bind(this);
		this.textBoxChange = this.textBoxChange.bind(this);
		this.renderPopularDestination = this.renderPopularDestination.bind(this);
		this.renderSuggestionListItem = this.renderSuggestionListItem.bind(this);
		this.destinationSelected = this.destinationSelected.bind(this);
		this.setNode = this.setNode.bind(this);
		this.checkMouseClick = this.checkMouseClick.bind(this);
		this.highlightWord = this.highlightWord.bind(this);
	}

	static getDerivedStateFromProps(props, state) {
		const tempAirportList = props.airportList || listData;
		const tempPopularList = props.popularList || popularData;
		if (props.value.airportCode !== state.selectedCity.airportCode) {
			return {
				selectedCity: props.value,
				airportList: tempAirportList,
				popularList: tempPopularList
			};
		}
		return {
			airportList: tempAirportList,
			popularList: tempPopularList
		};
	}

	setNode(node) {
		this.mainWrapper = node;
	}

	toggleSuggetion() {
		if (this.state.isSuggestionOpen === false && !this.props.isMobile) {
			document.body.addEventListener("click", this.checkMouseClick);
		} else {
			return 0;
		}

		this.setState(prev => ({
			isSuggestionOpen: true
		}));
	}

	checkMouseClick(event) {
		// mouseCheckOutside return true (mouse clicked outside target) & false (mouse clicked inside target)
		let value = mouseCheckOutside(event, this.mainWrapper);

		if (value) {
			this.setState(
				prev => ({
					isSuggestionOpen: !prev.isSuggestionOpen
				}),
				() => {
					document.body.removeEventListener("click", this.checkMouseClick);
				}
			);
		}
	}

	textBoxChange(evt) {
		const { id } = this.props;
		const { isPopularShow } = this.state;
		const target = evt.target;
		let temp = false;

		if (target.value.length < 1 && !isPopularShow) {
			temp = true;
		}

		this.setState({
			isPopularShow: temp,
			textBoxValue: target.value,
			selectedCity: { cityName: null, airportCode: null }
		});

		if (this.props.onTyping) this.props.onTyping(id, target.value);
	}

	highlightWord(word) {
		const { textBoxValue } = this.state;
		const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g;
		const regex = new RegExp(textBoxValue.replace(invalid, ""), "i");
		const target = word.match(regex);

		if (target === null) return word;
		const startIndex = target.index;

		return (
			<React.Fragment>
				{word.slice(0, startIndex)}
				<b>{word.slice(startIndex, startIndex + textBoxValue.length)}</b>
				{word.slice(startIndex + textBoxValue.length, word.length + 1)}
			</React.Fragment>
		);
	}

	renderSuggestionListItem() {
		const { highlightWord } = this;

		return this.state.airportList.map((country, index) => {
			return (
				<SuggestionItem key={index.toString()}>
					<div className="country">{country.countryName[0].value}</div>
					<div className="city_wrapper">
						{country.airports.length === 0 && (
							<div
								onClick={this.destinationSelected}
								data-airportcode={country.code}
								data-cityname={country.cityName[0].value}
							>
								{highlightWord(country.cityName[0].value)} (
								{highlightWord(country.code)}) -{" "}
								{highlightWord(country.name[0].value)}
							</div>
						)}
						{country.airports.length > 0 && (
							<React.Fragment>
								<div
									onClick={this.destinationSelected}
									data-airportcode={country.code}
									data-cityname={country.cityName[0].value}
								>
									{highlightWord(country.cityName[0].value)} (
									{highlightWord(country.code)}) - Semua Bandara
								</div>
								<ul>
									{country.airports.map(airport => {
										return (
											<li
												key={airport.id}
												onClick={this.destinationSelected}
												data-airportcode={airport.id}
												data-cityname={airport.name[0].value}
											>
												{highlightWord(country.cityName[0].value)} (
												{highlightWord(airport.id)}) -{" "}
												{highlightWord(airport.name[0].value)}
											</li>
										);
									})}
								</ul>
							</React.Fragment>
						)}
					</div>
				</SuggestionItem>
			);
		});
	}

	renderPopularDestination() {
		return this.state.popularList.map((country, index) => {
			return (
				<div key={index.toString()}>
					<PopularCountry>{country.countryName}</PopularCountry>
					<PopularCity>
						{country.cities.map((city, index) => {
							return (
								<div key={index.toString()}>
									<span
										onClick={this.destinationSelected}
										data-airportcode={city.id}
										data-cityname={city.name}
									>
										{city.name}
									</span>
								</div>
							);
						})}
					</PopularCity>
				</div>
			);
		});
	}

	destinationSelected(evt) {
		const airportCode = evt.target.dataset.airportcode;
		const cityName = evt.target.dataset.cityname;

		this.setState(
			{
				isSuggestionOpen: false,
				isPopularShow: true,
				textBoxValue: "",
				selectedCity: { cityName: cityName, airportCode: airportCode }
			},
			() => {
				document.body.removeEventListener("click", this.checkMouseClick);
				this.props.onChange(this.props.id, {
					cityName: cityName,
					airportCode: airportCode
				});
			}
		);
	}

	render() {
		const {
			maxHeight,
			maxWidth,
			suggestionWidth,
			column,
			right,
			isMobile
		} = this.props;
		const {
			isSuggestionOpen,
			isPopularShow,
			selectedCity,
			textBoxValue
		} = this.state;
		const {
			toggleSuggetion,
			textBoxChange,
			renderSuggestionListItem,
			renderPopularDestination,
			setNode
		} = this;

		// this.airportList = tempAirportList;

		let searchBoxValue = textBoxValue;
		if (selectedCity.cityName && selectedCity.airportCode) {
			searchBoxValue = `${selectedCity.cityName} (${selectedCity.airportCode})`;
		}

		if (!isMobile) {
			return (
				<MainWrapper ref={setNode}>
					<TextBox
						placeholder="Kota atau Bandara"
						onClick={toggleSuggetion}
						maxWidth={maxWidth}
						onChange={textBoxChange}
						value={searchBoxValue}
						type="text"
					/>
					<SearchSuggestion
						isOpen={isSuggestionOpen}
						maxHeight={maxHeight}
						right={right}
					>
						<PopularDestination isShow={isPopularShow}>
							<PopularTitle>Destinasi Populer</PopularTitle>
							<PopularContent width={suggestionWidth} maxHeight={maxHeight}>
								<ColumnWrapper column={column}>
									{renderPopularDestination()}
								</ColumnWrapper>
							</PopularContent>
						</PopularDestination>
						{!isPopularShow && (
							<SuggestionList width={suggestionWidth} maxHeight={maxHeight}>
								{renderSuggestionListItem()}
							</SuggestionList>
						)}
					</SearchSuggestion>
				</MainWrapper>
			);
		} else {
			return (
				<React.Fragment>
					<MobileInput
						type="text"
						placeholder="Pilih Kota atau Bandara"
						onClick={toggleSuggetion}
						onChange={textBoxChange}
						value={searchBoxValue}
					/>
					<MobileSuggestionWrapper>
						{/*---*/}
						<PopularDestination isShow={isPopularShow} isMobile>
							<PopularTitle isMobile>Destinasi Populer</PopularTitle>
							<PopularContent isMobile>
								<ColumnWrapper column={column}>
									{renderPopularDestination()}
								</ColumnWrapper>
							</PopularContent>
						</PopularDestination>
						{/*---*/}
						{!isPopularShow && (
							<SuggestionList>{renderSuggestionListItem()}</SuggestionList>
						)}
						{/*---*/}
					</MobileSuggestionWrapper>
				</React.Fragment>
			);
		}
	}
}

export default SearchCity;
