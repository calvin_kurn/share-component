import styled from "react-emotion";

const iconSearch =
	"url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAMAAACf4xmcAAAAhFBMVEUAAAB6enpmZmZnZ2dpaWlmZmZnZ2dra2tmZmZmZmZnZ2dnZ2dpaWlmZmZmZmZmZmZnZ2dmZmZmZmZoaGhpaWlmZmZnZ2dnZ2dnZ2doaGhmZmZqampsbGxnZ2dmZmZnZ2dmZmZnZ2dmZmZnZ2dmZmZmZmZmZmZnZ2dra2tnZ2dnZ2dmZmYh7jztAAAAK3RSTlMABuyyLfk1DOeZZlET9fHexqiEOx3TwaFgS0QmF7eSjHt018u7iWxYKth+1eGaKAAAAVhJREFUOMvVkkeSwyAURL8lo5xzsCw5h77//cbYZQkkXOOa3fSC0LyC5gP9Sd7VyH5BNk7ngytPjM/79AyAlh8s3hcfQDcA9Mrjw1Wz1QB7paDuQLeepmbiI9osqARsJzvrEDdzRl3A6tfIKeJh/cqao5OpjMF5Rzzu3/kzC/IJJUqxLmcLAx80CDwxBphcUrPElvc6esE9cVPW6ZnCgCV4N6Q0V/c8LxRWMmi0UMtOj7bHfXRSFLSUHj6aCqVQtFiBVWj5XY+j4cBWYCkafofD9OjK3TZwiWroo2EgIoXa2UEefJPUKsXniuCSWhraabJDrKZq7MW0PtRfOsdZ/pSR6kefEUq2aalKl7J5ZsNHQrTw+rl3YYhN+bEYoC+iNAEsZ3KNAog1BddGgGXXV3OVpcMB0Cpaqzhyc4wKEh5BzdF1OO4DFkZ2zRdnnFr/hrPpGy7c0pf6AS1MH5Pi09JvAAAAAElFTkSuQmCC)";

const MainWrapper = styled("div")`
	position: relative;
`;

const TextBox = styled("input")`
	box-sizing: border-box;
	padding: 8px;
	border-radius: 4px;
	border: 1px solid #e0e0e0;
	width: 100%;
	font-size: 14px;
	max-width: ${props => `${props.maxWidth}px`};
	height: 37px;
	cursor: pointer;
	font-weight: 400;
	color: rgba(0, 0, 0, 0.7);

	&:focus {
		outline: none;
	}

	&::placeholder {
		color: rgba(0, 0, 0, 0.38);
		opacity: 1;
	}

	&:-ms-input-placeholder {
		color: rgba(0, 0, 0, 0.38);
	}

	&::-ms-input-placeholder {
		color: rgba(0, 0, 0, 0.38);
	}
`;

const SearchSuggestion = styled("div")(
	`
	display: inline-block;
	position: absolute;
	top: calc(100% + 8px);
	overflow: hidden;
	background: #fff;
	border-radius: 4px;
	box-shadow: 0 8px 10px 0 rgba(0, 0, 0, 0.1), 0 -3px 10px 0 rgba(0, 0, 0, 0.1);
`,
	props => ({
		maxHeight: props.isOpen ? props.maxHeight + "px" : "0px",
		opacity: props.isOpen ? 1 : 0,
		transition: props.isOpen
			? "max-height 0.4s ease 0.1s, opacity 1s"
			: "max-height 0.4s , opacity 1s ease 0.3s",
		left: props.right ? null : 0,
		right: props.right ? 0 : null
	})
);

const PopularDestination = styled("div")`
	min-width: 100%;
	display: ${props => (props.isShow ? "initial" : "none")};
`;

const PopularTitle = styled("div")`
	font-size: 14px;
	font-weight: 600;
	padding: 16px;
	background: ${props => (props.isMobile ? "#f8f8f8" : null)};
	border-bottom: ${props => (props.isMobile ? null : "1px solid #e0e0e0")};
`;

// 68px on max height = 52px (header destination popular) + 16 padding
const PopularContent = styled("div")`
	overflow: auto;
	padding: 16px 16px 0 16px;
	background: ${props => (props.isMobile ? "#fff" : "#fcfcfc")};
	color: rgba(0, 0, 0, 0.7);
	font-size: 14px;
	width: ${props => props.width}px;
	max-height: ${props => props.maxHeight - 68}px;
	min-height: ${props => (props.isMobile ? "69.7vh" : null)};
`;

const ColumnWrapper = styled("div")`
	columns: ${props => props.column};
`;

const PopularCountry = styled("div")`
	font-weight: 600;
	margin-bottom: 4px;
	user-select: none;
`;

const PopularCity = styled("div")`
	margin-bottom: 24px;

	& > div {
		margin-bottom: 4px;

		span {
			width: 100%;
			cursor: pointer;
			color: rgba(0, 0, 0, 0.54);

			&:hover {
				color: #42b549;
			}
		}
	}
`;

const SuggestionList = styled("div")`
	display: inline-block;
	width: ${props => (props.width ? `${props.width}px` : "100%")};
	max-height: ${props => (props.maxHeight ? `${props.maxHeight}px` : "none")};
	overflow: auto;
`;

const SuggestionItem = styled("div")`
	.country {
		padding: 16px;
		background: #f8f8f8;
		font-weight: 600;
		font-size: 14px;
	}

	.city_wrapper {
		padding: 0 16px;
		margin: 16px 0;
		font-size: 14px;
		color: rgba(0, 0, 0, 0.54);

		& > div {
			cursor: pointer;

			&:hover {
				color: #42b549;
			}
		}

		ul {
			padding: 0 0 0 24px;
			margin: 0;
			list-style: none;

			li {
				margin: 8px 0;
				cursor: pointer;

				&:hover {
					color: #42b549;
				}
			}
		}

		b {
			color: rgba(0, 0, 0, 0.7);
		}
	}
`;

const MobileInput = styled("input")`
	width: calc(100% - 32px);
	font-size: 14px;
	line-height: 40px;
	border-radius: 4px;
	border: 1px solid #e0e0e0;
	background: #f8f8f8;
	padding: 0 16px 0 32px;
	background-image: ${iconSearch};
	background-size: 17px 17px;
	background-position: 8px center;
	background-repeat: no-repeat;
	margin: 16px;

	&:focus {
		outline: none;
	}

	&::placeholder {
		color: rgba(0, 0, 0, 0.38);
		opacity: 1;
	}

	&:-ms-input-placeholder {
		color: rgba(0, 0, 0, 0.38);
	}

	&::-ms-input-placeholder {
		color: rgba(0, 0, 0, 0.38);
	}
`;

const MobileSuggestionWrapper = styled("div")`
	border-top: 1px solid #e0e0e0;
	width: 100%;
	height: calc(100vh - 125px);
	overflow-y: auto;
`;

export {
	TextBox,
	MainWrapper,
	SearchSuggestion,
	PopularDestination,
	PopularTitle,
	PopularContent,
	PopularCountry,
	PopularCity,
	SuggestionList,
	SuggestionItem,
	ColumnWrapper,
	MobileInput,
	MobileSuggestionWrapper
};
