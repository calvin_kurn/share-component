const listData = [
	{
		id: "ID",
		name: "Indonesia",
		phoneCode: 62,
		cities: [
			{
				id: "57",
				name: "Jakarta",
				code: "JKTA",
				latitude: "-5.644444",
				longitude: "106.5625",
				airports: [
					{
						id: "CGK",
						name: "Soekarno-Hatta",
						aliases: ["Cengkareng", "Sukarno"]
					},
					{
						id: "HLP",
						name: "Halim Perdanakusuma",
						aliases: []
					}
				]
			}
		]
	},
	{
		id: "SG",
		name: "Singapore",
		phoneCode: 65,
		cities: [
			{
				id: "5185",
				name: "Singapore",
				code: "",
				latitude: "1.35019",
				longitude: "103.994003",
				airports: [
					{
						id: "SIN",
						name: "Changi",
						aliases: []
					}
				]
			},
			{
				id: "5184",
				name: "Seletar",
				code: "",
				latitude: "1.416949987",
				longitude: "103.8679962",
				airports: []
			}
		]
	}
];

const popularData = [
	{
		id: "ID",
		countryName: "Indonesia",
		cities: [
			{
				id: "CGK",
				airportName: "Soekarno-Hatta",
				name: "Jakarta"
			},
			{
				id: "DPS",
				airportName: "Ngurah Rai",
				name: "Denpasar"
			},
			{
				id: "SUB",
				airportName: "Juanda",
				name: "Surabaya"
			},
			{
				id: "JOG",
				airportName: "Adisutjipto",
				name: "Yogyakarta"
			},
			{
				id: "KNO",
				airportName: "Kualanamu",
				name: "Medan"
			},
			{
				id: "UPG",
				airportName: "Sultan Hasanuddin",
				name: "Makassar"
			},
			{
				id: "SRG",
				airportName: "Achmad Yani",
				name: "Semarang"
			},
			{
				id: "PDG",
				airportName: "Minangkabau",
				name: "Padang"
			},
			{
				id: "PLM",
				airportName: "Sultan Mahmud Badaruddin II",
				name: "Palembang"
			},
			{
				id: "PNK",
				airportName: "Supadio",
				name: "Pontianak"
			},
			{
				id: "BPN",
				airportName: "Sultan Aji Muhamad Sulaiman",
				name: "Balikpapan"
			},
			{
				id: "BDO",
				airportName: "Husein Sastranegara",
				name: "Bandung"
			}
		]
	},
	{
		id: "SG",
		countryName: "Singapore",
		cities: [
			{
				id: "SIN",
				airportName: "Changi",
				name: "Singapore"
			}
		]
	},
	{
		id: "MY",
		countryName: "Malaysia",
		cities: [
			{
				id: "KUL",
				airportName: "Kuala Lumpur",
				name: "Kuala Lumpur"
			},
			{
				id: "PEN",
				airportName: "Penang",
				name: "Penang"
			}
		]
	},
	{
		id: "TH",
		countryName: "Thailand",
		cities: [
			{
				id: "BKK",
				airportName: "Suvarnabhumi",
				name: "Bangkok"
			}
		]
	},
	{
		id: "HK",
		countryName: "Hong Kong",
		cities: [
			{
				id: "HKG",
				airportName: "Hong Kong",
				name: "Hong Kong"
			}
		]
	},
	{
		id: "JP",
		countryName: "Jepang",
		cities: [
			{
				id: "NRT",
				airportName: "Narita",
				name: "Tokyo"
			}
		]
	},
	{
		id: "KR",
		countryName: "Korea Selatan",
		cities: [
			{
				id: "ICN",
				airportName: "Incheon",
				name: "Seoul"
			}
		]
	},
	{
		id: "UK",
		countryName: "Inggris",
		cities: [
			{
				id: "LHR",
				airportName: "Heathrow",
				name: "London"
			}
		]
	}
];

export { listData, popularData };
