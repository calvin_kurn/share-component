import styled from "react-emotion";

const swapIconImage =
	"url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAdCAYAAAAkXAW5AAAA60lEQVR4Ae3WA2h/ARxF8fc3Zit7ec7Ysr3sxeXF2c5uttJs27az7YZz58z9vtOpT8Z9fs5rdHNzE47fjlEa4IdrTMPdckgl1CTcrEZ8Rw3UOP5bDqmDGrUe0gA1jH9WQ36gCWoQfy2HtEANWA75iTaoPvyxHNIB1YPfL18ukS4Uh02oLvzWiEtYlqwRiSg2UoAw56s31cNL5MTIHuI1YgqPneHUhfaRoBE+WIUqt7wkvliDKrX+LduAKrIc4o9NqALLIQHYhsq1HBKIHahsyyFB2IPKsBwSjH2oNMshoTiESrIcEoYFpDgftVs9xYXJQWpuqwAAAABJRU5ErkJggg==)";

const swapIconImageMobile =
	"url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAMAAABiM0N1AAAAsVBMVEUAAABmZmZgYGBmZmZnZ2doaGhnZ2doaGhnZ2dmZmZnZ2dnZ2dmZmZlZWVoaGhlZWVmZmZnZ2dmZmZmZmZmZmZoaGhmZmZnZ2f///9mZmaTk5PS0tJubm5ycnKOjo74+Pj19fXY2Njv7++7u7tqamrl5eV4eHjy8vKZmZmHh4ft7e2zs7Ph4eHPz8/MzMyrq6u2trZ/f3/b29ukpKTq6urIyMjGxsbBwcG/v7+EhISenp5qR98TAAAAGHRSTlMA8Qjf1FtSIbiRh0jXsHZlKMmd5sAxGaTXxkMqAAAC70lEQVRYw+2X2XLiMBBFMVvYMknIMn1Awh7bGAyEJdtA/v/DZqNYLNlyUvOY80hR11Lr3m6p8sUXeTw0Opf1Ww/vtn7ZaTx8UuW60+OcXuf64zLNG0AN1stx6IsfjpfzgQJumh+U6YF+fpJznp4V1LvlZS5aoCNfTPxIweVFSZ2uR7wKxE6wivHKLeobTEPJZzaAK7dMtQ9rKWYO7apL5w71KC6WMS2HUp9kLG4Wmr6jPmqv41KKC+vUhUcpxxK6+f7xWEtZ5ni5fmoxlfIMuMzLBXEo5ZnFOZur1pgb3nsrUFpRtwo10Jl4hQnPBUKBwtoL6mzljMmG1JcCIm4sOvfozAd3bAIpwldYOl2blZyRkjz+yLCYyCnPdEyhGgs5ZYiV3c/TTkfPNGN2ZyEkoyypgt1MDiiMiXB1akZ/H4JIDF41SXhiyoZZouhonySV37yBxUaTlPSYE7NILYbHMu9Xt7VmeKaOvy7NmNR4OawetV/8mnerfQayZ2ya2+PgmRGHk1nYjDRBHU/kNisEhz/GBFKIOvzBx8smlvj4QS1lhQTyVxRmhIq25uMV1CjGOmQPzXzLwKiR9dRG2LrQlP0RhIqlcWpWH72hJxZLw+vezakYPrI7e8cgsNkn/mvEjT6GbU2nKGsvmmRomxvxn199X4ys5aV/vAE9GmRJiIOsER6K+5EfbbDyJGL0I0eHnD0NM7wmpOLukPcuHwYbduc78zXX1inys7DTp2wmtininmvZbSRhZomapnPSmmxHM2PS9ipWGqiJlCeMybt03/Eu5ZnSyn00eERSlvXxfmTShKGU4xG6/+MOOVZ8qxTxvdytdpzQd9yzb1BD974Ud84b+3eInHWmXy31Fnkv8lM4ZV8fF00PNffzIjdXeN2yj9A70PYEbzW0LiqladRArxbZd8NKQ+1jj9Fqow7oaTR8Cf4E/WUYTTVQb1QrH+W+XeMfMf+ote8rn+Piqt2qeYBXa7WvXKVxU6188UUhvwCQPAC1HtwWzgAAAABJRU5ErkJggg==)";

const MainWrapper = styled("table")`
	position: relative;
	width: calc(100% + 4px);
	margin-left: -4px;

	td:nth-of-type(2) {
		text-align: center;
	}
`;

const SwapIcon = styled("div")`
	cursor: pointer;
	margin: auto;
	width: 30px;
	height: 30px;
	border-radius: 50%;
	background-color: ${props => (props.isMobile ? "transparent" : "#42b549")};
	background-size: ${props => (props.isMobile ? "100%" : "60%")};
	background-repeat: no-repeat;
	background-position: center;
	background-image: ${props =>
		props.isMobile ? swapIconImageMobile : swapIconImage};
	transition: 0.5s;
	transform: ${props => (props.isRotate ? "rotate(180deg)" : null)};
`;

//mobile
const MobileWrapper = styled("div")`
	position: relative;
`;

const MobileCity = styled("div")`
	position: relative;
	border-width: 0.5px 0 0.5px 0;
	border-style: solid;
	border-color: rgba(0, 0, 0, 0.12);

	& > div {
		display: inline-block;
		width: 50%;
		padding: 16px;

		&:last-child {
			text-align: right;
			border-left: 0.5px solid rgba(0, 0, 0, 0.12);
		}

		.title {
			margin-bottom: 4px;
			font-size: 12px;
			color: rgba(0, 0, 0, 0.38);
		}

		.airport_code {
			font-size: 20px;
			font-weight: 900;
			color: rgba(0, 0, 0, 0.7);
		}

		.city_name {
			font-size: 14px;
			color: rgba(0, 0, 0, 0.38);
		}
	}
`;

const SwapIconMobile = styled("span")`
	position: absolute;
	left: calc(50% - 15px);
	top: calc(50% - 15px);
`;

export { MainWrapper, SwapIcon, MobileCity, SwapIconMobile, MobileWrapper };
