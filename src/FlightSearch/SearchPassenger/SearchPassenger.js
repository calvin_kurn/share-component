import React from "react";
import mouseCheckOutside from "../helper.js";
import {
	MainWrapper,
	TextBox,
	PassengerMenu,
	MobileWrapper,
	PassengerMenuContent
} from "./SearchPassengerStyle";

// unify component
import BottomSheet from "unify-react-mobile/build/BottomSheet";

const AdultInc = "AdultInc";
const AdultDec = "AdultDec";
const ChildInc = "ChildInc";
const ChildDec = "ChildDec";
const InfantInc = "InfantInc";
const InfantDec = "InfantDec";

class SearchPassneger extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isPassengerOpen: false,
			passengerCount: props.passenger
		};

		this.toggleMenu = this.toggleMenu.bind(this);
		this.setNode = this.setNode.bind(this);
		this.checkMouseClick = this.checkMouseClick.bind(this);
		this.PassengerIncDec = this.PassengerIncDec.bind(this);
		this.checkDisable = this.checkDisable.bind(this);
	}

	static getDerivedStateFromProps(props, state) {
		let checkPassenger = props.passenger;

		if (checkPassenger) {
			if (
				checkPassenger.adult !== state.passengerCount.adult ||
				checkPassenger.child !== state.passengerCount.child ||
				checkPassenger.infant !== state.passengerCount.infant
			) {
				return {
					passengerCount: checkPassenger
				};
			}
		}
		return null;
	}

	setNode(node) {
		this.mainRef = node;
	}

	checkMouseClick(event) {
		// mouseCheckOutside return true (mouse clicked outside target) & false (mouse clicked inside target)
		let value = mouseCheckOutside(event, this.mainRef);

		if (value) {
			this.setState(
				prev => ({
					isPassengerOpen: false
				}),
				() => {
					document.body.removeEventListener("click", this.checkMouseClick);
				}
			);
		}
	}

	toggleMenu() {
		if (this.state.isPassengerOpen === false && !this.props.isMobile) {
			document.body.addEventListener("click", this.checkMouseClick);
		}

		this.setState(prev => ({
			isPassengerOpen: !prev.isPassengerOpen
		}));
	}

	// increase / decrease passenger qty, button
	PassengerIncDec(evt) {
		let tempPassengerCount = this.state.passengerCount;

		switch (evt.target.name) {
			case AdultInc:
				tempPassengerCount.adult += 1;
				break;
			case AdultDec:
				if (tempPassengerCount.infant === tempPassengerCount.adult)
					tempPassengerCount.infant -= 1;
				tempPassengerCount.adult -= 1;
				break;
			case ChildInc:
				tempPassengerCount.child += 1;
				break;
			case ChildDec:
				tempPassengerCount.child -= 1;
				break;
			case InfantInc:
				tempPassengerCount.infant += 1;
				break;
			case InfantDec:
				tempPassengerCount.infant -= 1;
				break;
			default:
				break;
		}

		this.setState(
			{
				passengerCount: tempPassengerCount
			},
			this.props.onChange(this.state.passengerCount)
		);
	}

	checkDisable(param) {
		const {
			passengerCount: { adult: adult, child: child, infant: infant }
		} = this.state;

		let result = false;

		switch (param) {
			case AdultInc:
				if (adult + child === 7) result = true;
				break;
			case AdultDec:
				if (adult === 1) result = true;
				break;
			case ChildInc:
				if (adult + child === 7) result = true;
				break;
			case ChildDec:
				if (child === 0) result = true;
				break;
			case InfantInc:
				if (infant === adult) result = true;
				break;
			case InfantDec:
				if (infant === 0) result = true;
				break;
			default:
				break;
		}

		return result;
	}

	render() {
		const { maxWidth, isMobile } = this.props;
		const { isPassengerOpen, passengerCount } = this.state;
		const {
			toggleMenu,
			setNode,
			PassengerIncDec,
			updatePassengerQty,
			mobileClick,
			checkDisable
		} = this;

		const passengerMenuContent = (
			<PassengerMenuContent isMobile={isMobile}>
				<tbody>
					<tr>
						<td>
							<div className="mobile_icon adult" />
							<span>
								<div className="title">Dewasa</div>
								<div className="subTitle">Lebih dari 12 tahun</div>
							</span>
						</td>
						<td>
							<button
								name={AdultDec}
								disabled={checkDisable(AdultDec)}
								onClick={PassengerIncDec}
							>
								-
							</button>
						</td>
						<td>
							<input type="tel" value={passengerCount.adult} readOnly />
						</td>
						<td>
							<button
								name={AdultInc}
								disabled={checkDisable(AdultInc)}
								onClick={PassengerIncDec}
							>
								+
							</button>
						</td>
					</tr>
					<tr>
						<td>
							<div className="mobile_icon child" />
							<span>
								<div className="title">Anak</div>
								<div className="subTitle">2 - 12 tahun</div>
							</span>
						</td>
						<td>
							<button
								name={ChildDec}
								disabled={checkDisable(ChildDec)}
								onClick={PassengerIncDec}
							>
								-
							</button>
						</td>
						<td>
							<input type="tel" value={passengerCount.child} readOnly />
						</td>
						<td>
							<button
								name={ChildInc}
								disabled={checkDisable(ChildInc)}
								onClick={PassengerIncDec}
							>
								+
							</button>
						</td>
					</tr>
					<tr>
						<td>
							<div className="mobile_icon infant" />
							<span>
								<div className="title">Bayi</div>
								<div className="subTitle">kurang dari 2 tahun</div>
							</span>
						</td>
						<td>
							<button
								name={InfantDec}
								disabled={checkDisable(InfantDec)}
								onClick={PassengerIncDec}
							>
								-
							</button>
						</td>
						<td>
							<input type="tel" value={passengerCount.infant} readOnly />
						</td>
						<td>
							<button
								name={InfantInc}
								disabled={checkDisable(InfantInc)}
								onClick={PassengerIncDec}
							>
								+
							</button>
						</td>
					</tr>
				</tbody>
			</PassengerMenuContent>
		);

		if (!isMobile) {
			return (
				<MainWrapper innerRef={setNode}>
					<TextBox maxWidth={maxWidth} onClick={toggleMenu}>
						{`${passengerCount.adult} Dewasa`}
						{passengerCount.child > 0 && `, ${passengerCount.child} Anak`}
						{passengerCount.infant > 0 && `, ${passengerCount.infant} Bayi`}
					</TextBox>
					<PassengerMenu isOpen={isPassengerOpen}>
						{passengerMenuContent}
					</PassengerMenu>
				</MainWrapper>
			);
		} else {
			return (
				<React.Fragment>
					<MobileWrapper onClick={toggleMenu}>
						<tbody>
							<tr>
								<td width="40px" rowSpan="2" className="icon_passenger" />
								<td className="title">Penumpang</td>
							</tr>
							<tr>
								<td className="content">
									{`${passengerCount.adult} Dewasa`}
									{passengerCount.child > 0 && `, ${passengerCount.child} Anak`}
									{passengerCount.infant > 0 &&
										`, ${passengerCount.infant} Bayi`}
								</td>
							</tr>
						</tbody>
					</MobileWrapper>
					<BottomSheet
						full
						padding="0"
						title="Kelas Penerbangan"
						onClose={toggleMenu}
						display={isPassengerOpen}
					>
						{passengerMenuContent}
					</BottomSheet>
				</React.Fragment>
			);
		}
	}
}

export default SearchPassneger;
