import React from "react";
import mouseCheckOutside from "../helper.js";
import {
	MainWrapper,
	TextBox,
	ClassList,
	ClassItem,
	MobileWrapper
} from "./SearchClassStyle";

// unify compoent
import BottomSheet from "unify-react-mobile/build/BottomSheet";
import List from "unify-react-mobile/build/List";

const classData = ["", "Ekonomi", "Bisnis", "Utama"];

class SearchClass extends React.Component {
	constructor() {
		super();

		this.state = {
			isClassOpen: false,
			selectedClass: 1
		};

		this.toggleClass = this.toggleClass.bind(this);
		this.setNode = this.setNode.bind(this);
		this.checkMouseClick = this.checkMouseClick.bind(this);
		this.changeClass = this.changeClass.bind(this);
		this.changeClassMobile = this.changeClassMobile.bind(this);
	}

	static getDerivedStateFromProps(props, state) {
		if (!props.passengerClass) return null;
		if (props.passengerClass !== state.selectedClass) {
			return {
				selectedClass: props.passengerClass
			};
		}
		return null;
	}

	setNode(node) {
		this.mainRef = node;
	}

	toggleClass() {
		if (this.state.isClassOpen === false && !this.props.isMobile) {
			document.body.addEventListener("click", this.checkMouseClick);
		}

		this.setState(prev => ({ isClassOpen: !prev.isClassOpen }));
	}

	checkMouseClick(event) {
		// mouseCheckOutside return true (mouse clicked outside target) & false (mouse clicked inside target)
		let value = mouseCheckOutside(event, this.mainRef);

		if (value) {
			this.setState(
				prev => ({
					isClassOpen: false
				}),
				() => {
					document.body.removeEventListener("click", this.checkMouseClick);
				}
			);
		}
	}

	changeClass(evt) {
		const id = evt.target.id;
		this.setState(
			{
				selectedClass: id,
				isClassOpen: false
			},
			this.props.onChange(parseInt(id))
		);
	}

	changeClassMobile(id) {
		this.setState(
			{
				selectedClass: id,
				isClassOpen: false
			},
			this.props.onChange(parseInt(id))
		);
	}

	render() {
		const { isMobile } = this.props;
		const { isClassOpen, selectedClass } = this.state;
		const {
			toggleClass,
			setNode,
			changeClass,
			mobileClick,
			changeClassMobile
		} = this;

		if (!isMobile) {
			return (
				<MainWrapper innerRef={setNode}>
					<TextBox onClick={toggleClass}>{classData[selectedClass]}</TextBox>
					<ClassList isOpen={isClassOpen}>
						<ClassItem onClick={changeClass} id="1" active={selectedClass == 1}>
							{classData[1]}
						</ClassItem>
						<ClassItem onClick={changeClass} id="2" active={selectedClass == 2}>
							{classData[2]}
						</ClassItem>
						<ClassItem onClick={changeClass} id="3" active={selectedClass == 3}>
							{classData[3]}
						</ClassItem>
					</ClassList>
				</MainWrapper>
			);
		} else {
			return (
				<React.Fragment>
					<MobileWrapper onClick={toggleClass}>
						<tbody>
							<tr>
								<td width="40px" rowSpan="2" className="icon_class" />
								<td className="title">Kelas</td>
							</tr>
							<tr>
								<td className="content">{classData[selectedClass]}</td>
							</tr>
						</tbody>
					</MobileWrapper>
					<BottomSheet
						full
						padding="0"
						title="Kelas Penerbangan"
						onClose={toggleClass}
						display={isClassOpen}
					>
						<List
							noMargin
							items={[
								{
									text: "Ekonomi",
									checked: selectedClass == 1,
									onClick: () => changeClassMobile(1)
								},
								{
									text: "Bisnis",
									checked: selectedClass == 2,
									onClick: () => changeClassMobile(2)
								},
								{
									text: "Utama",
									checked: selectedClass == 3,
									onClick: () => changeClassMobile(3)
								}
							]}
						/>
					</BottomSheet>
				</React.Fragment>
			);
		}
	}
}

export default SearchClass;
