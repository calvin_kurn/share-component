import styled from "react-emotion";

const iconClassMobile =
	"url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzNiIgaGVpZ2h0PSIzNiIgdmlld0JveD0iMCAwIDM2IDM2Ij4gICAgPGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4gICAgICAgIDxwYXRoIGZpbGw9IiNGRkYiIGQ9Ik0yOS4wMTkgMjAuNDcyYy0uNC0uNjktMS4wNzctMS4xMDQtMS44MDQtMS4xMDRsLTIuMzctLjE4NC0xLjA4OC0yLjY4Yy0uMzg0LS45MTEtMS4xNTUtMS41MzMtMi4wMzktMS42NDFsLTUuNzEtLjc0MWMtLjgyNS0uMTAyLTEuNTMtLjcxMy0xLjgzNi0xLjU5bC0yLjM5My02LjI5N2MtLjM4NC0xLjEwMy0xLjMyLTEuNzk4LTIuMzYzLTEuNzk4SDYuNTRjLS45MTggMC0yLjEwMiAxLjA1NS0xLjUgMi45NDJsNS40NzcgMTcuMjk4Yy4zMzEgMS4wNDYgMS4zMDMgMS43MTUgMi40MDEgMS43MTVoMTYuODI0YzEuMDggMCAxLjc1Mi0xLjE0MiAxLjIxLTIuMDc1bC0xLjkzMi0zLjg0NXoiLz4gICAgICAgIDxwYXRoIGZpbGw9IiNFMEUwRTAiIGQ9Ik0zMC41OCAyNS40NzdhLjk1NC45NTQgMCAwIDEtLjgzOS40NzdIMTIuOTE3Yy0uOTIgMC0xLjcxNi0uNTY2LTEuOTgzLTEuNDFMNS40NTUgNy4yNDhjLS4yMzItLjczMy0uMTc2LTEuMzg5LjE2LTEuODUuMjMyLS4zMTcuNTk1LS41MjIuOTI0LS41MjJoMi44NzhjLjg2NiAwIDEuNjMxLjU5IDEuOTUzIDEuNTE2bDIuMzg4IDYuMjg0Yy4wNzEuMjA1LjE2Ny4zOTMuMjcyLjU3bDEuOTQ2IDQuNjg3YTMuMTEgMy4xMSAwIDAgMCAyLjgwNSAxLjc0Mmg2LjcyOGwxLjcwNi4xMzJjLjU2NyAwIDEuMDk3LjMyNyAxLjQyLjg3NWwxLjkzNyAzLjg1NmEuOTE4LjkxOCAwIDAgMSAuMDA4Ljk0Ii8+ICAgICAgICA8cGF0aCBzdHJva2U9IiM2NjYiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgZD0iTTI5LjAxOSAyMC40NzJjLS40LS42OS0xLjA3Ny0xLjEwNC0xLjgwNC0xLjEwNGwtMi4zNy0uMTg0LTEuMDg4LTIuNjhjLS4zODQtLjkxMS0xLjE1NS0xLjUzMy0yLjAzOS0xLjY0MWwtNS43MS0uNzQxYy0uODI1LS4xMDItMS41My0uNzEzLTEuODM2LTEuNTlsLTIuMzkzLTYuMjk3Yy0uMzg0LTEuMTAzLTEuMzItMS43OTgtMi4zNjMtMS43OThINi41NGMtLjkxOCAwLTIuMTAyIDEuMDU1LTEuNSAyLjk0Mmw1LjQ3NyAxNy4yOThjLjMzMSAxLjA0NiAxLjMwMyAxLjcxNSAyLjQwMSAxLjcxNWgxNi44MjRjMS4wOCAwIDEuNzUyLTEuMTQyIDEuMjEtMi4wNzVsLTEuOTMyLTMuODQ1eiIvPiAgICAgICAgPHBhdGggc3Ryb2tlPSIjNjY2IiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIGQ9Ik0xMy43ODYgMTEuNTE2bDIuNTgyIDYuMjIzYTIuNjkyIDIuNjkyIDAgMCAwIDIuNDEyIDEuNDk4aDcuMDA3Ii8+ICAgICAgICA8cGF0aCBmaWxsPSIjRTBFMEUwIiBkPSJNMTIuNTk1IDMwLjU2OGMwIC41MTUuNDE1LjkzNC45My45MzRoMTUuNDlhLjk0Ljk0IDAgMCAwIC45NC0uOTM0VjI5LjA4aC0xNy4zNnYxLjQ4OHoiLz4gICAgICAgIDxwYXRoIHN0cm9rZT0iIzY2NiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIiBkPSJNMTIuNTk1IDMwLjU2OGMwIC41MTUuNDE1LjkzNC45My45MzRoMTUuNDlhLjk0Ljk0IDAgMCAwIC45NC0uOTM0VjI5LjA4aC0xNy4zNnYxLjQ4OHoiLz4gICAgPC9nPjwvc3ZnPg==)";

const MainWrapper = styled("div")`
	position: relative;
`;

const TextBox = styled("div")`
	padding: 8px;
	border-radius: 4px;
	border: 1px solid #e0e0e0;
	font-size: 14px;
	background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30"><path d="M3 10l12.004 12.004m12-12.004L15 22.004" fill="none" fill-rule="evenodd" stroke="%23999" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.25"/></svg>');
	background-size: 14px;
	background-repeat: no-repeat;
	background-position: 97% center;
	cursor: pointer;
	font-weight: 400;
	color: rgba(0, 0, 0, 0.7);
`;

const ClassList = styled("div")(
	`
	font-size: 14px;
	display: inline-block;
	position: absolute;
	top: calc(100% + 8px);
	overflow: hidden;
	background: #fff;
	border-radius: 4px;
	width: 250px;
	right: 0;
	box-shadow: 0 8px 10px 0 rgba(0, 0, 0, 0.1), 0 -3px 10px 0 rgba(0, 0, 0, 0.1);
`,
	props => ({
		maxHeight: props.isOpen ? "250px" : "0px",
		opacity: props.isOpen ? 1 : 0,
		transition: props.isOpen
			? "max-height 0.4s ease 0.1s, opacity 1s"
			: "max-height 0.4s , opacity 1s ease 0.3s"
	})
);

const ClassItem = styled("div")`
	padding-left: 48px;
	line-height: 48px;
	cursor: pointer;
	position: relative;

	&::before {
		content: "";
		display: inline-block;
		position: absolute;
		top: 14px;
		left: 16px;
		width: 16px;
		height: 16px;
		border-radius: 50%;
		border: 1px solid #42b549;
	}

	&::after {
		${props => (props.active ? 'content: ""' : null)};
		display: inline-block;
		width: 12px;
		height: 12px;
		position: absolute;
		top: 16px;
		left: 18px;
		background: #42b549;
		border-radius: 50%;
	}

	&:not(:last-child) {
		border-bottom: 1px solid #e0e0e0;
	}
`;

//mobile
const MobileWrapper = styled("table")`
	position: relative;
	width: 100%;
	border-bottom: 0.5px solid rgba(0, 0, 0, 0.12);
	padding: 0 16px;

	.title {
		padding-top: 16px;
		font-size: 12px;
		color: rgba(0, 0, 0, 0.38);
	}

	.content {
		padding-bottom: 16px;
		color: rgba(0, 0, 0, 0.54);
		font-size: 14px;
	}

	.icon_class {
		background-size: 32px 32px;
		background-repeat: no-repeat;
		background-position: left center;
		background-image: ${iconClassMobile};
	}
`;

export { MainWrapper, TextBox, ClassList, ClassItem, MobileWrapper };
