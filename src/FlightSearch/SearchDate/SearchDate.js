import React from "react";
import mouseCheckOutside from "../helper.js";
import {
	MainWrapper,
	TextBox,
	DateWrapper,
	NavigationWrapper,
	TextBoxContent,
	CheckBoxWrapper,
	CheckBox,
	//mobile
	MobileWrapper,
	MobileDateRow
} from "./SearchDateStyle";

import { localeMonth } from "./locale";

// import external component
import DateMonth from "./DateMonth";

// unify component
import Datepicker from "unify-react-mobile/build/Datepicker";

class SearchDate extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedFirstDate: props.date.departureDate,
			selectedSecondDate: props.date.returnDate,
			dateShow: props.date.departureDate,
			isDatepickerOpen: false,
			selectState: null, // 1 for first date || 2 for second date
			isRange: props.isRangePicker
		};

		this.dateChange = this.dateChange.bind(this);
		this.incMonth = this.incMonth.bind(this);
		this.decMonth = this.decMonth.bind(this);
		this.openDatepicker = this.openDatepicker.bind(this);
		this.checkMouseClick = this.checkMouseClick.bind(this);
		this.setNode = this.setNode.bind(this);
		this.toggleRoundTrip = this.toggleRoundTrip.bind(this);
		this.closeDatePickerMobile = this.closeDatePickerMobile.bind(this);
	}

	static getDerivedStateFromProps(props, state) {
		let checkDepartureDate = false;
		let checkReturnDate = false;

		if (props.date) {
			checkDepartureDate =
				props.date.departureDate.getTime() !==
				state.selectedFirstDate.getTime();
			checkReturnDate =
				props.date.returnDate.getTime() !== state.selectedSecondDate.getTime();
		}

		if (
			(props.isRangePicker !== state.isRange && props.isMobile) ||
			(checkDepartureDate || checkReturnDate)
		) {
			return {
				isRange: props.isRangePicker,
				selectedFirstDate: props.date.departureDate,
				selectedSecondDate: props.date.returnDate
			};
		}
		return null;
	}

	dateChange(newDate) {
		const {
			selectedFirstDate,
			selectedSecondDate,
			selectState,
			isDatepickerOpen,
			isRange
		} = this.state;
		let tempFirstDate = selectState == 1 ? newDate : selectedFirstDate;
		let tempSecondDate = selectState == 2 ? newDate : selectedSecondDate;
		let tempIsOpen = selectState == 1 ? true : false;
		let tempSelectState = selectState == 1 ? 2 : null;

		// if single date
		if (!isRange) {
			tempFirstDate = tempSecondDate = newDate;
			tempIsOpen = false;
			tempSelectState = null;
		} else if (
			tempFirstDate.getTime() > tempSecondDate.getTime() ||
			tempSecondDate.getTime() < tempFirstDate.getTime()
		) {
			tempFirstDate = new Date(newDate);
			tempSecondDate = new Date(newDate);
			tempIsOpen = true;
			tempSelectState = 2;
		}

		this.setState(
			{
				selectedFirstDate: tempFirstDate,
				selectedSecondDate: tempSecondDate,
				selectState: tempSelectState,
				isDatepickerOpen: tempIsOpen
			},
			this.props.onChange(tempFirstDate, tempSecondDate, this.state.isRange)
		);
	}

	incMonth() {
		this.setState(prev => ({
			dateShow: new Date(
				prev.dateShow.getFullYear(),
				prev.dateShow.getMonth() + 1,
				1
			)
		}));
	}

	decMonth() {
		this.setState(prev => ({
			dateShow: new Date(
				prev.dateShow.getFullYear(),
				prev.dateShow.getMonth() - 1,
				1
			)
		}));
	}

	openDatepicker(evt) {
		if (this.state.isDatepickerOpen === false && !this.props.isMobile) {
			document.body.addEventListener("click", this.checkMouseClick);
		}

		this.setState({
			selectState: evt.currentTarget.id,
			isDatepickerOpen: true
		});
	}

	setNode(node) {
		this.mainRef = node;
	}

	checkMouseClick(event) {
		// mouseCheckOutside return true (mouse clicked outside target) & false (mouse clicked inside target)
		let value = mouseCheckOutside(event, this.mainRef);

		if (value) {
			this.setState(
				prev => ({
					isDatepickerOpen: false,
					selectState: null
				}),
				document.body.removeEventListener("click", this.checkMouseClick)
			);
		}
	}

	toggleRoundTrip() {
		this.setState(
			prev => ({
				isRange: !prev.isRange
			}),
			this.props.onChange(
				this.state.selectedFirstDate,
				this.state.selectedFirstDate,
				!this.state.isRange
			)
		);
	}

	// mobile handler
	closeDatePickerMobile(evt) {
		this.setState({
			selectState: null,
			isDatepickerOpen: false
		});
	}

	render() {
		const { isMobile } = this.props;
		const {
			selectedFirstDate,
			selectedSecondDate,
			dateMove,
			dateShow,
			isRange,
			isDatepickerOpen,
			selectState
		} = this.state;
		const {
			dateChange,
			incMonth,
			decMonth,
			openDatepicker,
			setNode,
			toggleRoundTrip,
			closeDatePickerMobile
		} = this;

		if (!isMobile) {
			return (
				<MainWrapper innerRef={setNode}>
					<CheckBoxWrapper>
						<span>Berangkat</span>
						<span className="round_trip">
							<CheckBox active={isRange} onClick={toggleRoundTrip} />{" "}
							<span onClick={toggleRoundTrip}>Pulang</span>
						</span>
					</CheckBoxWrapper>
					<TextBox>
						<TextBoxContent
							active={selectState == 1}
							id={1}
							onClick={openDatepicker}
						>
							{`${selectedFirstDate.getDate()} ${
								localeMonth.idShort[selectedFirstDate.getMonth()]
							} ${selectedFirstDate.getFullYear()}`}
						</TextBoxContent>
						{isRange && (
							<TextBoxContent
								active={selectState == 2}
								id={2}
								onClick={openDatepicker}
							>
								{`${selectedSecondDate.getDate()} ${
									localeMonth.idShort[selectedSecondDate.getMonth()]
								} ${selectedSecondDate.getFullYear()}`}
							</TextBoxContent>
						)}
					</TextBox>
					<DateWrapper isOpen={isDatepickerOpen}>
						<NavigationWrapper>
							<button onClick={decMonth}>←</button>
							<button onClick={incMonth}>→</button>
						</NavigationWrapper>
						<DateMonth
							selectedFirstDate={selectedFirstDate}
							selectedSecondDate={selectedSecondDate}
							dateShow={dateShow}
							changeDate={dateChange}
							dateMove={dateMove}
						/>
						<DateMonth
							selectedFirstDate={selectedFirstDate}
							selectedSecondDate={selectedSecondDate}
							dateShow={
								new Date(dateShow.getFullYear(), dateShow.getMonth() + 1, 1)
							}
							changeDate={dateChange}
							dateMove={dateMove}
						/>
					</DateWrapper>
				</MainWrapper>
			);
		} else {
			return (
				<React.Fragment>
					<MobileWrapper>
						<MobileDateRow onClick={openDatepicker} id={1}>
							<tbody>
								<tr>
									<td className="icon_departure" rowSpan="2" width="40px" />
									<td className="title">Tanggal Pergi</td>
								</tr>
								<tr>
									<td className="content">{`${selectedFirstDate.getDate()} ${
										localeMonth.idShort[selectedFirstDate.getMonth()]
									} ${selectedFirstDate.getFullYear()}`}</td>
								</tr>
							</tbody>
						</MobileDateRow>
						{isRange && (
							<MobileDateRow onClick={openDatepicker} id={2}>
								<tbody>
									<tr>
										<td className="icon_return" rowSpan="2" width="40px" />
										<td className="title">Tanggal Pulang</td>
									</tr>
									<tr>
										<td className="content">{`${selectedSecondDate.getDate()} ${
											localeMonth.idShort[selectedSecondDate.getMonth()]
										} ${selectedSecondDate.getFullYear()}`}</td>
									</tr>
								</tbody>
							</MobileDateRow>
						)}
					</MobileWrapper>
					<Datepicker
						full
						minDate={new Date()}
						maxDate={
							new Date(new Date().setFullYear(new Date().getFullYear() + 1))
						}
						firstDate={selectedFirstDate}
						secondDate={isRange ? selectedSecondDate : selectedFirstDate}
						onClick={dateChange}
						onClickDouble={() => {}}
						title={
							selectState == 1 ? "Pilih Tanggal Pergi" : "Pilih Tanggal Pulang"
						}
						onClose={closeDatePickerMobile}
						display={isDatepickerOpen}
					/>
				</React.Fragment>
			);
		}
	}
}

export default SearchDate;
