import styled from "react-emotion";

const checkBoxImage =
	"url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEIAAABCCAMAAADUivDaAAAAmVBMVEUAAABFukxDuEtFt0xCtktCtklCtklCtUpCtklCtUlEtktNuVNNuVROuVVPulVRu1dSu1hTu1lTu1pUvFpZvl9avmBiwWhjwmlqxG9qxHCu37HC58TL6s3O68/W79jX79nY79nZ8NrZ8Nva8Nzb8dzd8d7e8t/f8uDf8uHg8+Hh8+Li8+Po9ujo9unv+PDw+fDz+vT2+/f///+2f8+5AAAACXRSTlMAJT1DmsDY7vMIooM4AAABKElEQVR4AaTBiQ0DMAhFsUc44O+/cKtIHaDB5vKo0Z+mwvmx1KM0rtN61ocvay20AamVBNeSE1oKSkvFaGnQ0qcd+9ZBGIqhMDxfUgKB0Amh94Lf/+EYIEeWoiscyxLLnbx9s//jXCACYUBE84mSgFDSc6wkIBAMFREtiWCoiPgjEJVaAsKlryTiyiOAUAog9IKciFdMUBFJLZxzJyHkgpxI1kwQEcW04xdERHGjGTcgnCB4CQjEjXRTCz0nIwZXIhhNQUJURNyAcITwkxjeuZFuuSAkuCEQQHiMHELXiYim8freAwQpAcMnCAg3ggFBTsCQCiD8xj5zCgIGhJYEjAcEBQFjB6ElAWOR/fU5CEQgDJLGIKwM8s4gMg1S1yC4DbLfYHwwmEDePq7FfiaL6NEAAAAASUVORK5CYII=)";

const iconDeparture =
	"url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAMAAABiM0N1AAAAxlBMVEUAAABmZmZmZmZpaWllZWVnZ2dnZ2dmZmZnZ2dmZmZmZmZzc3NmZmZmZmZnZ2dnZ2dra2tmZmZnZ2dmZmZmZmZnZ2doaGhmZmZnZ2dpaWlqampmZmb////q6upmZmaBgYG6urqkpKSZmZmenp7JycmIiIjx8fGSkpLp6elzc3Pk5OR8fHz09PTS0tJxcXFpaWn7+/v39/fMzMyxsbFsbGz9/f3t7e3f39+Li4vX19enp6eWlpaPj494eHjY2NjAwMC2traDg4OwWzLFAAAAHHRSTlMAVXhe/v76/vW/kQfY0HZKG+euiXttVi2BRDUoln9RygAAAZ9JREFUWMPt1tduwjAYhuF0JKHstowuvpSEANlQ9uq4/5sqfxDCrWScgA8qNc9BfllCr2IbJJRMJpPezR0974sFhVEo3tO4u0neKUCjUUJTYTRRoqGB8gLVRvlyqwaNxi3yl4w8bmloqNEoN6r8Tj2HFHJ1XqcCfd2OQaVnCLPNMBHSULFbrnVUeEeMUSvWR0TDQ9BiBPBoRHB36xF4x56jj8QMm56W47Ih17Fo2MZ+jRwnBLR+6vOXBBCEuLKQzJCRCjekIyWd+0avqRzZ2ksqWehvh2a+nNAUm6mU0GQOrytla0Mbhi/lsH0D9lDKrXU9zAdSrn+6QW9wYmhhsVZjWCeGevjFOTHU7bCWKj5knFEQ4VPGYb+PMZJx/bMxljK+kG4IS8ZPZOjAnMgI+eFqcNhle27vnLA15nUCFdjT+CExD2bnbavjQb06IzTE2I3/dPWoc0aoC+fQOTtEnWvqcGjwk4SEHaWIRZKQsKPkYSUJCTvKs45AHAoFHfKAaDQQhSDu0OYQfZnH9OKOWKUEAeokUn28OKr8pGQy/8g348QKMOwiD7wAAAAASUVORK5CYII=)";

const iconReturn =
	"url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAMAAABiM0N1AAAAw1BMVEUAAABmZmZnZ2dpaWllZWVoaGhnZ2dnZ2dmZmZnZ2dzc3NmZmZmZmZmZmZra2tmZmZnZ2dmZmZnZ2dmZmZnZ2dnZ2dnZ2dmZmZqamr////q6upmZmaBgYG6urqkpKSZmZmenp7IyMiJiYnx8fGSkpLo6Oh8fHxwcHDk5OTS0tJzc3P8/Pzz8/PMzMywsLB0dHRra2v39/ft7e3m5ubY2NiVlZX19fXe3t6np6ePj4+GhoZ4eHj5+fng4ODBwcG0tLSFhYW9d3ABAAAAGXRSTlMAVXde/v72+9ZJB4+GKxvnwruuemzLlHM1maWr1gAAAZZJREFUWMPt1llvgkAUhmG1sriv3fyoIFiWagH3rdv//1XtgTZgkxHQuWhS3os5OVw8MZJJKOTl5WWv06WzLlfjD6tynUa3k96pQqIhoxF/2oBMQwTxCTWLJaoHiUYLtVKsGlo0JPSCtdhkO30RGRL7LKcC7IZBAJ0W1GEsFRYNAeG6AyoMqA13ELSAQMOEM4jlwKThYxHuLtoMSIQ9CFN0OjXPjkO2p9HQlZ8dIgMCBsc9slcKSICY5RBPSMkUG8pamQk9ZApgQveZyqG/C003fKD5AXMekGFC5/GLpgr2Mw7QbA9lyuHPftFhGhze2mSMw5zH69fgr7R4yzMhD78anwm9AdtRPOMciHqH4PC5Ii78JZ+7toW14XNpNVg2n0urwptxgSYr6/iSvA717zJBRB1tDqIkNpScCXX0/NXIhHB1AbSGvwg+usbkXAAZeIqciyFyyuQwErFOB4UOOxnLNFDksLqBlgaKHFYNCU4yZCU4VB2CO0mCQE5SNUD4UE+1SuFQlWskRE6qmpXiyW7vCnl5/6hPn37vTdU9KKgAAAAASUVORK5CYII=)";

const MainWrapper = styled("div")`
	position: relative;
`;

const TextBox = styled("div")`
	padding: 8px 0;
	border: 1px solid #e0e0e0;
	border-radius: 4px;
	display: flex;
	font-size: 14px;
	cursor: pointer;
	font-weight: 400;
	color: rgba(0, 0, 0, 0.7);
`;

const TextBoxContent = styled("div")(
	`
	flex: 1;
	padding-left: 8px;

	&:first-of-type:not(:last-child) {
		border-right: 1px solid #e0e0e0;
	}
`,
	props => ({
		color: props.active ? "#42b549" : null
	})
);

const DateWrapper = styled("div")(
	`
	position: absolute;
	top: calc(100% + 8px);
	overflow: hidden;
	background: #fff;
	border-radius: 4px;
	display: flex;
	box-shadow: 0 8px 10px 0 rgba(0, 0, 0, 0.1), 0 -3px 10px 0 rgba(0, 0, 0, 0.1);

	& > div:not(:first-of-type) {
		padding: 16px;
	}
`,
	props => ({
		maxHeight: props.isOpen ? "500px" : "0px",
		opacity: props.isOpen ? 1 : 0,
		transition: props.isOpen
			? "max-height 0.4s ease 0.1s, opacity 1s"
			: "max-height 0.4s , opacity 1s ease 0.3s"
	})
);

const NavigationWrapper = styled("div")`
	position: absolute;
	display: flex;
	justify-content: space-between;
	left: 16px;
	right: 16px;
	top: 16px;

	button {
		font-size: 20px;
		line-height: 24px;
		background: none;
		border: none;
		z-index: 60;
		outline: none;
		cursor: pointer;
		color: rgba(0, 0, 0, 0.54);
	}
`;

const CheckBoxWrapper = styled("div")`
	display: flex;
	padding-bottom: 6px;

	& > * {
		flex: 1;
		display: flex;
		align-items: center;
	}

	& > .round_trip > span {
		cursor: pointer;
		user-select: none;
	}
`;

const CheckBox = styled("div")`
	position: relative;
	display: inline-block;
	width: 14px;
	height: 14px;
	margin-right: 4px;
	border-radius: 4px;
	border-style: solid;
	border-width: 1px;
	border-color: ${props => (props.active ? "#42b549" : "#e0e0e0")};
	background: ${props => (props.active ? checkBoxImage : "#fff")};
	background-size: 100%;
	cursor: pointer;
`;

//mobile
const MobileWrapper = styled("div")`
	position: relative;
`;

const MobileDateRow = styled("table")`
	width: 100%;
	padding: 0 16px;
	border-bottom: 0.5px solid rgba(0, 0, 0, 0.12);

	.title {
		padding-top: 16px;
		font-size: 12px;
		color: rgba(0, 0, 0, 0.38);
	}

	.content {
		padding-bottom: 16px;
		color: rgba(0, 0, 0, 0.54);
		font-size: 14px;
	}

	.icon_departure {
		background-size: 32px 32px;
		background-repeat: no-repeat;
		background-position: left center;
		background-image: ${iconDeparture};
	}

	.icon_return {
		background-size: 32px 32px;
		background-repeat: no-repeat;
		background-position: left center;
		background-image: ${iconReturn};
	}
`;

export {
	MainWrapper,
	TextBox,
	DateWrapper,
	NavigationWrapper,
	TextBoxContent,
	CheckBoxWrapper,
	CheckBox,
	MobileWrapper,
	MobileDateRow
};
