import React from "react";
import { MonthWrapper, DayTable } from "./DateMonthStyle";
import { localeDay, localeMonth } from "./locale";

class DateMonth extends React.Component {
	constructor() {
		super();

		this.initialProcess = true;
		this.selectDay = this.selectDay.bind(this);
	}

	shouldComponentUpdate(nextProps) {
		const { selectedFirstDate, selectedSecondDate, dateShow } = this.props;

		const selectedFirstDateNew = new Date(nextProps.selectedFirstDate);
		selectedFirstDateNew.setHours(0, 0, 0, 0);

		const selectedSecondDateNew = new Date(nextProps.selectedSecondDate);
		selectedSecondDateNew.setHours(0, 0, 0, 0);

		// only re-render when selected date || show date change
		if (
			selectedFirstDate.getTime() !== selectedFirstDateNew.getTime() ||
			selectedSecondDate.getTime() !== selectedSecondDateNew.getTime() ||
			dateShow !== nextProps.dateShow
		) {
			return true;
		}
		return false;
	}

	getDay() {
		const { selectDay } = this;
		const { selectedFirstDate, selectedSecondDate, dateShow } = this.props;

		const today = new Date();
		today.setHours(0, 0, 0, 0);

		const year = dateShow.getFullYear();
		const month = dateShow.getMonth();

		let qty = new Date(year, month + 1, 0).getDate();
		let start = new Date(year, month, 1).getDay();

		let temp = [];
		let currDate = 1;

		for (let i = 0; i < 6; i++) {
			let tempI = [];

			if (currDate > qty) break;

			for (let x = 0; x < 7; x++) {
				let currIndex = x + 1 + i * 7;
				let dateValue = "";

				// `curr >` using greater than so day move forward
				// 6 -> saturday, on render it will take 7th position
				if (currIndex > start && currDate <= qty) {
					dateValue = currDate;

					const currDateObj = new Date(year, month, dateValue);
					const isPast = currDateObj < today;
					let isSelected = null;

					if (
						currDateObj.getTime() === selectedFirstDate.getTime() ||
						currDateObj.getTime() === selectedSecondDate.getTime()
					) {
						isSelected = "calendar_selected";
					} else if (
						currDateObj.getTime() > selectedFirstDate.getTime() &&
						currDateObj.getTime() < selectedSecondDate.getTime()
					) {
						isSelected = "calendar_between";
					}

					let tempX = (
						<td
							onClick={isPast ? null : selectDay}
							month={month}
							year={year}
							date={dateValue}
							key={currIndex}
							className={[isPast ? "disabled" : null, isSelected].join(" ")}
						>
							{dateValue}
						</td>
					);

					tempI.push(tempX);
					currDate++;
				} else {
					tempI.push(<td key={currIndex} className="empty" />);
				}
			}

			temp.push(<tr key={i.toString()}>{tempI}</tr>);
		}
		return temp;
	}

	selectDay(evt) {
		const year = evt.target.getAttribute("year");
		const month = evt.target.getAttribute("month");
		const date = evt.target.getAttribute("date");

		this.props.changeDate(new Date(year, month, date));
	}

	render() {
		const { dateShow, selectedFirstDate } = this.props;
		const { nextMonth, prevMonth } = this;

		return (
			<MonthWrapper>
				<DayTable>
					<thead>
						<tr>
							<td colSpan="7">
								{`${
									localeMonth.idFull[dateShow.getMonth()]
								} ${dateShow.getFullYear()}`}
							</td>
						</tr>
						<tr>
							<td>{localeDay.idShort[0]}</td>
							<td>{localeDay.idShort[1]}</td>
							<td>{localeDay.idShort[2]}</td>
							<td>{localeDay.idShort[3]}</td>
							<td>{localeDay.idShort[4]}</td>
							<td>{localeDay.idShort[5]}</td>
							<td>{localeDay.idShort[6]}</td>
						</tr>
					</thead>
					<tbody>{this.getDay()}</tbody>
				</DayTable>
			</MonthWrapper>
		);
	}
}

export default DateMonth;
