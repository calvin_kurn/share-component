const localeDay = {
	idShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
	idFull: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
	enShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
	enFull: ["Sunday", "Monday", "Wednesday", "Thursday", "Friday", "Saturday"]
};

const localeMonth = {
	idShort: [
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"Mei",
		"Jun",
		"Jul",
		"Agu",
		"Sep",
		"Okt",
		"Nov",
		"Des"
	],
	idFull: [
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember"
	],
	enShort: [
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"May",
		"Jun",
		"Jul",
		"Aug",
		"Sep",
		"Oct",
		"Nov",
		"Dec"
	],
	enFull: [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	]
};

export { localeDay, localeMonth };
