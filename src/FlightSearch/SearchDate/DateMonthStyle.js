import styled from "react-emotion";

const MonthWrapper = styled("div")`
	position: relative;
	display: inline-block;
`;

const DayTable = styled("table")`
	width: 100%;
	font-size: 14px;

	td {
		text-align: center;
	}

	thead {
		td {
			line-height: 24px;
			font-weight: 600;
		}

		tr:first-child {
			td {
				padding-bottom: 4px;
				color: rgba(0, 0, 0, 0.7);
			}
		}

		tr:last-child {
			font-size: 11px;

			td:first-of-type {
				color: red;
			}
		}
	}

	tbody {
		td {
			padding: 8px;
			border-radius: 4px;
			background: transparent;
			transition: 0.5s ease;
			user-select: none;
			color: rgba(0, 0, 0, 0.54);

			&.calendar_selected {
				background: #42b549;
				color: #fff;
			}

			&.calendar_between {
				background: #d3f0d4;
			}

			&.disabled {
				opacity: 0.2;
				cursor: initial;
			}

			&:not(.empty):not(.disabled) {
				cursor: pointer;

				&:hover {
					background: rgba(0, 0, 0, 0.12);
				}
			}
		}

		td:first-of-type {
			color: red;
		}
	}
`;

export { MonthWrapper, DayTable };
