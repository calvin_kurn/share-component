import FlightSearch from "./FlightSearch";
import listData from "./SearchCity/airport";

const FlightSearchNote = {
  notes: `
    <b>FlightSearch</b><br/>

    <b style="color: red">Props List</b>:
    <b>ClassChanged</b> = <font style="color: #42b549">function(newClass)</font><br/>
    newClass -> Int (1 = Ekonomi | 2 = Bisnis | 3 = Utama) <br/><br/>

    <b>destinationChanged</b> = <font style="color: #42b549">function(newDestination)</font><br/>
    newDestination -> Object<br/><br/>

    <b>destinationTyping</b> = <font style="color: #42b549">function(index,value)</font><br/>
    newClass -> Int (1 = From [Dari] | 2 = To [Tujuan]) <br/>
    value -> string <br/><br/>

    <b>dateChanged</b> = <font style="color: #42b549">function(newDepartureDate, newReturnDate, rangeFlag)</font><br/>
    newDepartureDate -> JS Date Object<br/>
    newReturnDate -> JS Date Object &nbsp; <font style="color: red">*returnDate always return a value even on single trip, check the flag if single/round trip</font> 
    rangeFlag -> Boolean<br/><br/>

    <b>passengerChanged</b> = <font style="color: #42b549">function(newPassenger)</font><br/>
    newPassenger -> <b>Object</b><br/><br/>

    <b>submitValue</b> = <font style="color: #42b549">function(value,rangeFlag)</font><br/>
    <pre>
    rangeFlag - true => round trip
              - false => single trip
              
    value = {
      passengerClass: Int,
      date: {
          departureDate: JS Date Object,
          returnDate: JS Date Object &nbsp; <font style="color: red">*returnDate always return a value even on single trip, check the flag if single/round trip</font><br/>
        },
      passenger: { adult: Int , child: Int , infant: Int  },
      destination: {
          departure: { airportCode: String, cityName: String },
          return: { airportCode: String, cityName: String }
        }
    }
    </pre>

    <b>airportList</b> = <font style="color: #42b549">Array of Object*</font><br/>
    <b>popularList</b> = <font style="color: #42b549">Array of Object*</font>
    <pre style="margin: 0">
    *airportList = [
      {
        airports: [
          {
            id: "CGK",
            name: [
              {
                key: 0,
                value: "Soekarno-Hatta"
              }
            ]
          },
          {
            id: "HLP",
            name: [
              {
                key: 0,
                value: "Halim Perdanakusuma"
              }
            ]
          }
        ],
        cityName: [
          {
            key: 0,
            value: "Jakarta"
          }
        ],
        cityID: 57,
        code: "JKTA",
        countryID: "ID",
        countryName: [
          {
            key: 0,
            value: "Indonesia"
          }
        ],
        name: [
          {
            key: 0,
            value: "Jakarta"
          }
        ]
      },
      {
        airports: [],
        cityName: [
          {
            key: 0,
            value: "Jakar"
          }
        ],
        cityID: 1112,
        code: "BUT",
        countryID: "BT",
        countryName: [
          {
            key: 0,
            value: "Bhutan"
          }
        ],
        name: [
          {
            key: 0,
            value: "Bathpalathang"
          }
        ]
      },
      {
        airports: [],
        cityName: [
          {
            key: 0,
            value: "Jacmel"
          }
        ],
        cityID: 3022,
        code: "JAK",
        countryID: "HT",
        countryName: [
          {
            key: 0,
            value: "Haiti"
          }
        ],
        name: [
          {
            key: 0,
            value: "Jacmel"
          }
        ]
      }
    ]

    ====================================================

    *popularList = [
      {
        id: "SG",
        countryName: "Singapore",
        cities: [
          {
            id: "SIN",
            airportName: "Changi",
            name: "Singapore"
          }
        ]
      },
      {
        id: "MY",
        countryName: "Malaysia",
        cities: [
          {
            id: "KUL",
            airportName: "Kuala Lumpur",
            name: "Kuala Lumpur"
          },
          {
            id: "PEN",
            airportName: "Penang",
            name: "Penang"
          }
        ]
      },
      {
        id: "TH",
        countryName: "Thailand",
        cities: [
          {
            id: "BKK",
            airportName: "Suvarnabhumi",
            name: "Bangkok"
          }
        ]
      }
    ];
    </pre>

    ==========================================================================================
    props for set default value.

    <b>defaultPassengerClass</b> = <font style="color: #42b549">Int</font> (1 = 'Ekonomi' | 2 = 'Bisnis' | 3 = 'Utama')

    <b>defaultDate</b> = <font style="color: #42b549">Object</font><br/>
    {
      &nbsp;&nbsp; departureDate: JS Date object,
      &nbsp;&nbsp; returnDate: JS Date object,
    }

    <b>defaultDestination</b> = <font style="color: #42b549">Object</font><br/>
    {
      &nbsp;&nbsp; departure: { airportCode: "CGK", cityName: "Jakarta" },
      &nbsp;&nbsp; return: { airportCode: "DPS", cityName: "Bali" }
    }

    <b>defaultPassenger</b> = <font style="color: #42b549">Object</font><br/>
    { adult: 1, child: 0, infant: 0 }

    ==========================================================================================

    <b style="color:red">*add this props will change the component behaviour from uncontrol to control.
    - component value will follow the props value.
    - you must update the value / variable on your side.</b><br/>

    <b>passengerClass</b> = <font style="color: #42b549">Int</font> (1 = 'Ekonomi' | 2 = 'Bisnis' | 3 = 'Utama')

    <b>date</b> = <font style="color: #42b549">Object</font><br/>
    {
      &nbsp;&nbsp; departureDate: JS Date object,
      &nbsp;&nbsp; returnDate: JS Date object,
    }

    <b>destination</b> = <font style="color: #42b549">Object</font><br/>
    {
      &nbsp;&nbsp; departure: { code: "CGK", name: "Jakarta" },
      &nbsp;&nbsp; return: { code: "DPS", name: "Bali" }
    }

    <b>passenger</b> = <font style="color: #42b549">Object</font><br/>
    { adult: 1, child: 0, infant: 0 }
  `
};

export { FlightSearch, FlightSearchNote };
