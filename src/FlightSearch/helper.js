// check if mouse click is outside the target or not;
function mouseCheckOutside(event, target) {
	let mouseClickLocation = event.target;
	do {
		if (mouseClickLocation == target) {
			return false;
		}
		mouseClickLocation = mouseClickLocation.parentNode;
	} while (mouseClickLocation);
	return true;
}

export default mouseCheckOutside;
