import styled from "react-emotion";

const MainWrapper = styled("div")`
	max-width: 1200px;
	box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.1);
	border-radius: 6px;
	margin: auto;
	padding: 24px;
	background-color: #fff;
`;

const Button = styled("div")`
	display: ${props => props.display || "block"};
	background: #fa591d radial-gradient(circle, transparent 1%, #fa591d 1%)
		center/15000%;
	color: #fff;
	line-height: 44px;
	text-align: center;
	border-radius: 8px;
	letter-spacing: 0.4px;
	font-size: 14px;
	font-weight: 600;
	width: ${props => props.width};
	margin: ${props => (props.isMobile ? "18px" : null)};
	float: ${props => props.float};
	cursor: pointer;
	transition: background 0.8s;

	&:active {
		background-color: #ffa48b;
		background-size: 100%;
		transition: background 0s;
	}

	// &:hover {
	// 	background: #ef430c;
	// }
`;

const LayoutTable = styled("table")`
	width: 100%;
	border-collapse: collapse;

	& > tbody > tr {
		&:last-child > td:last-child {
			padding-top: 24px;
		}

		& > td:not(:last-child) {
			padding-right: 16px;
		}

		& > td {
			padding-top: 4px;
		}

		th {
			color: rgba(0, 0, 0, 0.54);
			font-size: 14px;
			text-align: left;
			font-weight: normal;

			& > * {
				vertical-align: middle;
			}

			.checkbox_wrapper {
				user-select: none;
				cursor: pointer;
			}

			&:not(:last-child) {
				padding-right: 16px;
			}
		}
	}
`;

//mobile
const MobileWrapper = styled("div")`
	position: relative;
`;

const ChipWrapper = styled("div")`
	position: relative;
	text-align: center;
	margin: 16px 0;

	& > .unf-chip {
		font-size: 14px;
		margin: 0;
		user-select: none;

		&:first-of-type {
			margin-right: 8px;
		}
	}
`;

export { MainWrapper, Button, LayoutTable, MobileWrapper, ChipWrapper };
