import unifyWrapper from "./HOC";

// Travel global component
import { FlightSearch, FlightSearchNote } from "./FlightSearch";

// Unify component <desktop mobile>
import UnifyMobileButton from "unify-react-mobile/build/Button";
import UnifyDesktopButton from "unify-react-desktop/build/Button";
const Button = unifyWrapper(UnifyMobileButton, UnifyDesktopButton);

import UnifyMobileCard from "unify-react-mobile/build/Card";
import UnifyDesktopCard from "unify-react-desktop/build/Card";
const Card = unifyWrapper(UnifyMobileCard, UnifyDesktopCard);

import UnifyMobileChip from "unify-react-mobile/build/Chip";
import UnifyDesktopChip from "unify-react-desktop/build/Chip";
const Chip = unifyWrapper(UnifyMobileChip, UnifyDesktopChip);

import UnifyMobileTab from "unify-react-mobile/build/Tab";
import UnifyDesktopTab from "unify-react-desktop/build/Tab";
const Tab = unifyWrapper(UnifyMobileTab, UnifyDesktopTab);

export { FlightSearch, FlightSearchNote, Button, Card, Chip, Tab };
