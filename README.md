### Tokopedia Travel
Travel react component library

## Changelog
- 1.1.*
	1. Integrate HOC for unify component (Desktop & Mobile).

- 1.0.*
	1. Initial component (Flight widget)