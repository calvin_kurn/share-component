import React from "react";
import { render } from "react-dom";
import { FlightSearch } from "./src";

const App = () => <FlightSearch />;
render(<App />, document.getElementById("root"));
